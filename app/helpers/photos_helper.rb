module PhotosHelper

  def photos_fullsize?
    controller_name == 'photos' && action_name == 'fullsize'
  end

  def photos_index?
  	controller_name == 'photos' && action_name == 'index'
  end

end
