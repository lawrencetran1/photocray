module UsersHelper

	def get_user_total_shots(user)
		user.photos.count
	end

	def get_user_total_followers(user)
		user.followers.count
	end	

	def get_user_total_likes(user)
		count = 0;
		user.photos.each do |photo|
			count += photo.likes.count
		end
		return count
	end

	def get_user_likes_last30(user)
		count = 0;
		user.photos.each do |photo|
			count += photo.likes.where(["created_at > ?", 30.days.ago]).count
		end
		return count
	end

	def get_user_total_comments(user)
		count = 0;
		user.photos.each do |photo|
			count += photo.comments.count
		end
		return count
	end

	def get_user_comments_last30(user)
		count = 0;
		user.photos.each do |photo|
			count += photo.comments.where(["created_at > ?", 30.days.ago]).count
		end
		return count
	end

	def get_user_total_views(user)
		count = 0;
		user.photos.each do |photo|
			count += photo.impressionist_count
		end
		return count
	end

	def get_user_views_last30(user)
		count = 0;
		user.photos.each do |photo|
			count += photo.impressionist_count(:start_date => Time.now - 30.days)
		end
		return count
	end

	def get_total_project_count(user)
		count = 0;
		user.projects.each do |project|
			count += project.impressionist_count
		end
		return count
	end

  def is_following_page?
  	controller_name == 'users' && action_name == 'following'
  end

  def get_fav_photographers(user)
  	fav_ids = []
  	user.follows.each do |fav|
  		fav_ids << fav.followed_id
		end

		#Query users where id is in the fav_ids array, join likes table, order by likes_count
		User.where(:id => fav_ids).select("users.*, SUM(photos.likes_count) AS likes_count").joins(:photos).group("users.id").order("likes_count DESC").limit(3)
  end

  def get_fans(user)
  	fan_ids = []
  	user.followers.each do |fan|
  		fan_ids << fan.follower_id
		end

		#Query users where id is in the fav_ids array, join likes table, order by likes_count
		User.where(:id => fan_ids).select("users.*, SUM(photos.likes_count) AS likes_count").joins(:photos).group("users.id").order("likes_count DESC").limit(3)	
  end

  # This method is used to create the start and end dates below
  def get_month(init, num)
  	if init == 'start'

      # Calculate beginning of the month date minus number of months from input
  		Date.new((Time.now-num.months).year, (Time.now-num.months).month, 1)
		elsif init == 'end'

      # Calculate end of the month date minus number of months from input
			Date.new((Time.now-num.months).year, (Time.now-num.months).month, -1)
		elsif init == 'now'

      # Today's date
			month6_end = Date.new(Time.now.year, Time.now.month, Time.now.day)
  	end
  end

  def get_reviews_per_month(user)

    # Initialize date variables
  	m0start, m1start, m2start, m3start, m4start, m5start, m6start, m0end, m1end, m2end, m3end, m4end, m5end, m6end = get_month('start', 6), get_month('start', 5), get_month('start', 4), get_month('start', 3), get_month('start', 2), get_month('start', 1), get_month('start', 0), get_month('end', 6), get_month('end', 5), get_month('end', 4), get_month('end', 3), get_month('end', 2), get_month('end', 1), Date.new(Time.now.year, Time.now.month, Time.now.day)

    # Return an array of arrays, each sub array is comprised of the month and number of reviews for that month
  	@reviews = [
  		[ m0start.strftime("%B"), user.reviews.where(:created_at => m0start..m0end).count ],
  		[ m1start.strftime("%B"), user.reviews.where(:created_at => m1start..m1end).count ],
  		[ m2start.strftime("%B"), user.reviews.where(:created_at => m2start..m2end).count ],
  		[ m3start.strftime("%B"), user.reviews.where(:created_at => m3start..m3end).count ],
  		[ m4start.strftime("%B"), user.reviews.where(:created_at => m4start..m4end).count ],
  		[ m5start.strftime("%B"), user.reviews.where(:created_at => m5start..m5end).count ],
  		[ m6start.strftime("%B"), user.reviews.where(:created_at => m6start..m6end).count ]
  	]
  end

  def get_bookings_per_month(user)

    # Initialize date variables
  	m0start, m1start, m2start, m3start, m4start, m5start, m6start, m0end, m1end, m2end, m3end, m4end, m5end, m6end = get_month('start', 6), get_month('start', 5), get_month('start', 4), get_month('start', 3), get_month('start', 2), get_month('start', 1), get_month('start', 0), get_month('end', 6), get_month('end', 5), get_month('end', 4), get_month('end', 3), get_month('end', 2), get_month('end', 1), get_month('end', 0)

  	# Return an array of arrays, each sub array is comprised of the month and number of bookings for that month
    @bookings = [
  		[ m0start.strftime("%B"), user.bookings.where(:created_at => m0start..m0end).count ],
  		[ m1start.strftime("%B"), user.bookings.where(:created_at => m1start..m1end).count ],
  		[ m2start.strftime("%B"), user.bookings.where(:created_at => m2start..m2end).count ],
  		[ m3start.strftime("%B"), user.bookings.where(:created_at => m3start..m3end).count ],
  		[ m4start.strftime("%B"), user.bookings.where(:created_at => m4start..m4end).count ],
  		[ m5start.strftime("%B"), user.bookings.where(:created_at => m5start..m5end).count ],
  		[ m6start.strftime("%B"), user.bookings.where(:created_at => m6start..m6end).count ]
  	]
  end

  def get_profile_views_per_month(user)

    # Initialize date variables
  	m0start, m1start, m2start, m3start, m4start, m5start, m6start, m0end, m1end, m2end, m3end, m4end, m5end, m6end = get_month('start', 6), get_month('start', 5), get_month('start', 4), get_month('start', 3), get_month('start', 2), get_month('start', 1), get_month('start', 0), get_month('end', 6), get_month('end', 5), get_month('end', 4), get_month('end', 3), get_month('end', 2), get_month('end', 1), get_month('end', 0)

  	# Return an array of arrays, each sub array is comprised of the month and number of profile views for that month
    @profile_views = [
  		[ m0start.strftime("%B"), user.impressionist_count(:start_date => m0start, :end_date => m0end) ],
  		[ m1start.strftime("%B"), user.impressionist_count(:start_date => m1start, :end_date => m1end) ],
  		[ m2start.strftime("%B"), user.impressionist_count(:start_date => m2start, :end_date => m2end) ],
  		[ m3start.strftime("%B"), user.impressionist_count(:start_date => m3start, :end_date => m3end) ],
  		[ m4start.strftime("%B"), user.impressionist_count(:start_date => m4start, :end_date => m4end) ],
  		[ m5start.strftime("%B"), user.impressionist_count(:start_date => m5start, :end_date => m5end) ],
  		[ m6start.strftime("%B"), user.impressionist_count(:start_date => m6start, :end_date => m6end) ]
  	]
  end

  def get_project_views_per_month(user)

    # Initialize date variables and project count variables for each date
  	m0start, m1start, m2start, m3start, m4start, m5start, m6start, m0end, m1end, m2end, m3end, m4end, m5end, m6end, projViewCountm0, projViewCountm1, projViewCountm2, projViewCountm3, projViewCountm4, projViewCountm5, projViewCountm6 = get_month('start', 6), get_month('start', 5), get_month('start', 4), get_month('start', 3), get_month('start', 2), get_month('start', 1), get_month('start', 0), get_month('end', 6), get_month('end', 5), get_month('end', 4), get_month('end', 3), get_month('end', 2), get_month('end', 1), get_month('end', 0), 0, 0, 0, 0, 0, 0, 0

    # Loop through user's projects and increment corresponding project count variable
  	user.projects.each do |project|
  		projViewCountm0 += project.impressionist_count(:start_date => m0start, :end_date => m0end)
  		projViewCountm1 += project.impressionist_count(:start_date => m1start, :end_date => m1end)
  		projViewCountm2 += project.impressionist_count(:start_date => m2start, :end_date => m2end)
  		projViewCountm3 += project.impressionist_count(:start_date => m3start, :end_date => m3end)
  		projViewCountm4 += project.impressionist_count(:start_date => m4start, :end_date => m4end)
  		projViewCountm5 += project.impressionist_count(:start_date => m5start, :end_date => m5end)
  		projViewCountm6 += project.impressionist_count(:start_date => m6start, :end_date => m6end)
		end

    # Return an array of arrays, each sub array is comprised of the month and number of project views for that month
  	@project_views = [
  		[ m0start.strftime("%B"), projViewCountm0 ],
  		[ m1start.strftime("%B"), projViewCountm1 ],
  		[ m2start.strftime("%B"), projViewCountm2 ],
  		[ m3start.strftime("%B"), projViewCountm3 ],
  		[ m4start.strftime("%B"), projViewCountm4 ],
  		[ m5start.strftime("%B"), projViewCountm5 ],
  		[ m6start.strftime("%B"), projViewCountm6 ]
  	]
  end

  def get_shots_per_month(user)

    # Initialize date variables and shot count variables for each date
    m0start, m1start, m2start, m3start, m4start, m5start, m6start, m0end, m1end, m2end, m3end, m4end, m5end, m6end, shotCountm0, shotCountm1, shotCountm2, shotCountm3, shotCountm4, shotCountm5, shotCountm6 = get_month('start', 6), get_month('start', 5), get_month('start', 4), get_month('start', 3), get_month('start', 2), get_month('start', 1), get_month('start', 0), get_month('end', 6), get_month('end', 5), get_month('end', 4), get_month('end', 3), get_month('end', 2), get_month('end', 1), get_month('end', 0), 0, 0, 0, 0, 0, 0, 0

    # Loop through user's shots and increment corresponding shot count variable
    user.photos.each do |shot|
      shotCountm0 += shot.impressionist_count(:start_date => m0start, :end_date => m0end)
      shotCountm1 += shot.impressionist_count(:start_date => m1start, :end_date => m1end)
      shotCountm2 += shot.impressionist_count(:start_date => m2start, :end_date => m2end)
      shotCountm3 += shot.impressionist_count(:start_date => m3start, :end_date => m3end)
      shotCountm4 += shot.impressionist_count(:start_date => m4start, :end_date => m4end)
      shotCountm5 += shot.impressionist_count(:start_date => m5start, :end_date => m5end)
      shotCountm6 += shot.impressionist_count(:start_date => m6start, :end_date => m6end)
    end

    # Return an array of arrays, each sub array is comprised of the month and number of shot views for that month
    @shots = [
      [ m0start.strftime("%B"), shotCountm0 ],
      [ m1start.strftime("%B"), shotCountm1 ],
      [ m2start.strftime("%B"), shotCountm2 ],
      [ m3start.strftime("%B"), shotCountm3 ],
      [ m4start.strftime("%B"), shotCountm4 ],
      [ m5start.strftime("%B"), shotCountm5 ],
      [ m6start.strftime("%B"), shotCountm6 ]
    ]
  end

  # Get user's shot that has the most likes
  def popular_shot_alltime(user)
    user.photos.length > 0 ? user.photos.order(:likes_count => :desc).first : nil
  end

  def popular_shot_last30days(user)

    # Initialize start and end date variables
    today = Date.new(Time.now.year, Time.now.month, Time.now.day)
    last30 = Date.new((Time.now-30.days).year, (Time.now-30.days).month, (Time.now-30.days).day)

    # Get the id of the photo that has the most likes in the last 30 days
    id = Photo.joins(:likes).where(user_id:user.id).where(:likes => {:created_at => last30..today}).group('photos.id').count.sort{|a,b| b[1] <=> a[1]}

    # Return photo after getting the id from above
    id.length > 0 ? Photo.find(id.first[0]) : nil
  end

  def popular_shot_last90days(user)

    # Initialize start and end date variables
    today = Date.new(Time.now.year, Time.now.month, Time.now.day)
    last90 = Date.new((Time.now-90.days).year, (Time.now-90.days).month, (Time.now-90.days).day)

    # Get the id of the photo that has the most likes in the last 90 days
    id = Photo.joins(:likes).where(user_id:user.id).where(:likes => {:created_at => last90..today}).group('photos.id').count.sort{|a,b| b[1] <=> a[1]}

    # Return photo after getting the id from above
    id.length > 0 ? Photo.find(id.first[0]) : nil
  end

  def popular_shot_last365days(user)

    # Initialize start and end date variables
    today = Date.new(Time.now.year, Time.now.month, Time.now.day)
    last365 = Date.new((Time.now-365.days).year, (Time.now-365.days).month, (Time.now-365.days).day)

    # Get the id of the photo that has the most likes in the last 90 days
    id = Photo.joins(:likes).where(user_id:user.id).where(:likes => {:created_at => last365..today}).group('photos.id').count.sort{|a,b| b[1] <=> a[1]}

    # Return photo after getting the id from above
    id.length > 0 ? Photo.find(id.first[0]) : nil
  end 

  # Returns the total number of likes for a photo
  def get_shot_likes_count(photo)
    photo.likes_count
  end

  # Returns the total number of comments for a photo
  def get_shot_comments_count(photo)
    photo.comments_count
  end

  # Returns the total number of views for a photo
  def get_shot_views_count(photo)
    photo.impressions_count
  end

  def get_booking_status(user, currentUser)
    # get accepted status of booking, if true then allow user to leave a review
    if currentUser
      booking_status = Booking.where(:user_id => user.id).where(:requestor_id => currentUser.id).exists?
    end

    if currentUser && booking_status
      accepted_bookings = Booking.where(:user_id => user.id).where(:requestor_id => currentUser.id).where(:accepted_status => true)
      @accepted_status = accepted_bookings.length > 0
    end    
  end

end
