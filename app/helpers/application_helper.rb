module ApplicationHelper
  def bootstrap_class_for flash_type
    case flash_type
      when :success
        "alert-success"
      when :error
        "alert-error"
      when :alert
        "alert-block"
      when :notice
        "alert-info"
      else
        flash_type.to_s
    end
  end	

  def user_total_bookings(user)
    user.bookings.count + Booking.where(:requestor_id => user.id).count
  end

  def user_total_reviews(user)
    user.reviews.count
  end

  def user_total_following(user)
    user.follows.count
  end

  def user_total_followers(user)
    user.followers.count
  end

  def user_total_projects(user)
    user.projects.count
  end

  def user_activities(user)
    user.activities.order(:created_at => :desc).limit(15)
  end

  def is_photos_page?
    controller_name == 'photos' && action_name == 'index'
  end

  def is_photographers_page?
    controller_name == 'users' && action_name == 'index'
  end

  def is_meetups_page?
    controller_name == 'home' && action_name == 'meetups'
  end

  def is_jobs_page?
    controller_name == 'home' && action_name == 'jobs'
  end

  def is_about_page?
    controller_name == 'home' && action_name == 'about'
  end  

  def is_contests_page?
    controller_name == 'home' && action_name == 'contests'
  end  

  def is_blog_page?
    controller_name == 'home' && action_name == 'blog'
  end

  def asset_url(asset)
    "#{request.protocol}#{request.host_with_port}#{asset_path(asset)}"
  end  
      
end
