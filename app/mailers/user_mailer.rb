class UserMailer < ApplicationMailer
  default from: #'larry@photocray.com'
 
  def get_quote(email_to, email_from, subject, message)
    @email_to = email_to
    @email_from = email_from
    @subject = subject
    @message = message
    mail(to: @email_to, from: @email_from, subject: @subject)
  end	
end
