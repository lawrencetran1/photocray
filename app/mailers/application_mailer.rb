class ApplicationMailer < ActionMailer::Base
  default from: "larry@photocray.com"
  layout 'mailer'
end
