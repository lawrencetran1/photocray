$(document).ready(function() {

	//turn to inline mode
	$.fn.editable.defaults.mode = 'popup';

	//overwrite default button settings
	$.fn.editableform.buttons = 
	  '<button type="submit" class="btn-primary btn-sm editable-submit">'+
	    '<i class="glyphicon glyphicon-ok"></i>'+
	  '</button>'+
	  '<button type="button" class="btn-primary btn-sm editable-cancel">'+
	    '<i class="glyphicon glyphicon-remove"></i>'+
	  '</button>';

	$('.reply-body').editable({
    title: 'Enter Reply',
    tpl: "<textarea style='width: 280px; height: 280px'>"
  });

	var disabled_dates = [];

  var array_dates = $('#dates_booked').data('dates');
  if (array_dates) {
	  array_dates.forEach(function(obj){
	      if (obj.accepted_status === true) {
	          var date_booked = new Date(obj.date_booked)
	          var month = date_booked.getMonth() + 1;
	          var day = date_booked.getDate();
	          var year = date_booked.getFullYear();
	          disabled_dates.push(month + '-' + day + '-' + year);
	      }
	  });	  	
  }

  $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

	$("#upload_avatar_link").on('click', function(e){
    e.preventDefault();
    $("#upload_avatar:hidden").trigger('click');
	});	

	$(".reply-form").hide();

	$(".reply-form-toggle").on('click', function(e){
		e.preventDefault();
    $(this).next().toggle();
	});	

	$("#upload_hero_bg_link").on('click', function(e){
    e.preventDefault();
    $("#upload_hero_bg:hidden").trigger('click');
	});

  $("#datepicker2").datepicker({
      dateFormat: 'mm/dd/yy',
      minDate: new Date(), 
      beforeShowDay: function(d) {
          var dmy = (d.getMonth()+1); 
          if(d.getMonth()<9) 
              dmy="0"+dmy; 
              dmy+= "-"; 
          
          if(d.getDate()<10) dmy+="0"; 
              dmy+=d.getDate() + "-" + d.getFullYear(); 
          
          if ($.inArray(dmy, disabled_dates) != -1) {
              return [false, "",""]; 
          } else {
              return [true,"",""]; 
          }
      }
  });	

  $("#datepicker2").on('change', function(){
  	$("#request-photographer").removeAttr("disabled");
  });

});