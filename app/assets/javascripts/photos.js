$(document).ready(function() {

	//turn to inline mode
	$.fn.editable.defaults.mode = 'popup';

	//overwrite default button settings
	$.fn.editableform.buttons = 
	  '<button type="submit" class="btn-primary btn-sm editable-submit">'+
	    '<i class="glyphicon glyphicon-ok"></i>'+
	  '</button>'+
	  '<button type="button" class="btn-primary btn-sm editable-cancel">'+
	    '<i class="glyphicon glyphicon-remove"></i>'+
	  '</button>';


	$('.dropdown-toggle').dropdown();

	$('.photo-title').editable();
	$('.photo-description').editable({
    title: 'Enter Description',
    tpl: "<textarea style='width: 280px; height: 280px'>"
  });
  
});