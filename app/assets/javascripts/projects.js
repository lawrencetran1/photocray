//turn to inline mode
$.fn.editable.defaults.mode = 'inline';

//overwrite default button settings
$.fn.editableform.buttons = 
  '<button type="submit" class="btn-primary btn-sm editable-submit">'+
    '<i class="glyphicon glyphicon-ok"></i>'+
  '</button>'+
  '<button type="button" class="btn-primary btn-sm editable-cancel">'+
    '<i class="glyphicon glyphicon-remove"></i>'+
  '</button>';

$(document).ready(function() {
  $('.project-title').editable();
  $('.project-category').editable({
        source: [
              // value corresponds to options_for_select in project.rb model, spelling must match
              {value: 'Weddings', text: 'Weddings'},
              {value: 'Birthdays', text: 'Birthdays'},
              {value: 'Events', text: 'Events'},
              {value: 'Fashion', text: 'Fashion'},
              {value: 'Auto', text: 'Auto'},
              {value: 'Landscape', text: 'Landscape'},
              {value: 'Product', text: 'Product'},
              {value: 'Portrait', text: 'Portrait'},
              {value: 'Food', text: 'Food'},
              {value: 'Uncategorized', text: 'Uncategorized'}
           ]
    });

  var minImageWidth = 1600,
      minImageHeight = 1200;

  Dropzone.options.mediaDropzone = {
    maxFilesize: 10, // Mb
    acceptedFiles: "image/*",

    init: function() {
      this.on("thumbnail", function(file) {
        if (file.width < minImageWidth || file.height < minImageHeight) {
          file.rejectDimensions()
        }
        else {
          file.acceptDimensions();
        }
      });
    },

    accept: function(file, done) {
      file.acceptDimensions = done;
      file.rejectDimensions = function() { done("Dimension needs to be at least 1600px by 1200px"); };
    }
  };
  

});