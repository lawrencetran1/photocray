$(function(){

	smoothTarget();
	searchForm();
	offcanvas();
	sliderConfig();
	autocompleteConfig();
	datepickerConfig();
    selectdropdownConfig();
	tooltipConfig();
	attachmentConfig();
	chartJS();
	submitForm();

});

/* 
* Add Smooth Scrolling Transition
*/

var smoothTarget = function(){

	$('a.target[href^="#"]').click(function(event) {

		var id 		= $(this).attr("href");
		var target 	= $(id).offset().top - 50;

		$('html, body').animate({ scrollTop:target }, 500);

		event.preventDefault();
	});

}

/*
* Search --------- */

var searchForm = function(){

	var btn 	= 	$('#search-btn'),
		form 	= 	$('.search-form'),
		body 	=	$(document.body);

	btn.click(function(event){
		form.toggleClass('search-visible').find('.search-field').val('').focus();
		return false;
		event.preventDefault();
	});	

	$(document).keydown(function(e){

        if(e.keyCode == 27) {
            form.removeClass('search-visible').addClass('search-hidden');
        }
    });

    $('.search-field').click(function(event){
    	return false;
    	event.preventDefault();
    })

    body.click(function(event){
		form.removeClass('search-visible').addClass('search-hidden');
	});
}


/*
* Offcanvas --------- */

var offcanvas = function(){

	var btn 		=	$('.navbar-toggle'),
		overlay 	=	$('.menu-overlay');
		body 		=	$(document.body);


	overlay.click(function(event){

		body.removeClass('offcanvas-open');
		event.preventDefault();
	});
	

	btn.click(function(event){
		body.toggleClass('offcanvas-open');

		return false;
		event.preventDefault();
	});

}

/*
* Slider --------- */

var sliderConfig = function(){

    $("#slider-rates").change(function () {                    
        var newValue = $('#slider-rates').val();
        $("#label-rates").html("Hourly Rate: $" + newValue + " and less");
    });    
}

/*
* Autocomplete --------- */

var autocompleteConfig = function(){

    var availableAddress = [
		"Los Angeles",
        "San Francisco",
        "Huntington Beach"
    ];

    $( ".address" ).autocomplete({
      source: availableAddress
    });

}

/*
* Date Picker --------- */

var datepickerConfig = function(){
    $( "#datepicker1" ).datepicker({
        minDate: '0'
    }); 
} 

/*
* Dropdown --------- */

var selectdropdownConfig = function(){

	var $menu 		= 	$('.menu'),
		dropdown  	= 	$menu.find('.dropdown-menu');

	dropdown.find('li a').click(function(event){
		var val = $(this).text();

		$(this).parents('.dropdown').find('.dropdown-label').text(val);


		event.preventDefault();
	})

}

/*
* Tooltip --------- */

var tooltipConfig = function(){
	$('[data-toggle="tooltip"]').tooltip()
}

/*
* Attachment --------- */

var attachmentConfig = function(){
	var $dock 	= $('#viewer'),
		thumb	= $dock.find('.viewer-img img');

		thumb.click(function(){
			$(this).parent('.viewer-img').toggleClass('zoom');

			if( $(this).parent('.viewer-img').hasClass('zoom') ){
				$('#column-viewer').attr('class', 'col-sm-12');
				$('#column-details').hide();
			}else{
				$('#column-viewer').attr('class', 'col-sm-9 col-sm-push-3 col-md-10 col-md-push-2');
				$('#column-details').show();
			}
		});
}

/*
* Chart JS --------- */

function chartJS(){

    $(document).ready(function() {

        var reviews = $('#data-reviews').data('stats'),
            bookings = $('#data-bookings').data('stats'),
            profileViews = $('#data-profile-views').data('stats'),
            projectViews = $('#data-project-views').data('stats'),
            shots = $('#data-shots').data('stats');

        if (shots) {
            var data = {
                labels: [shots[0][0], shots[1][0], shots[2][0], shots[3][0], shots[4][0], shots[5][0], shots[6][0]],
                datasets: [
                     {
                        label: "My First dataset",
                        fillColor: "#E6C6BB",
                        strokeColor: "#cfb2a8",
                        pointColor: "#b89e95",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [shots[0][1], shots[1][1], shots[2][1], shots[3][1], shots[4][1], shots[5][1], shots[6][1]]
                    }
                    // {
                    //     label: "My Second dataset",
                    //     fillColor: "#E3B6EF",
                    //     strokeColor: "#C99DD5",
                    //     pointColor: "#BF99C9",
                    //     pointStrokeColor: "#fff",
                    //     pointHighlightFill: "#fff",
                    //     pointHighlightStroke: "rgba(220,220,220,1)",
                    //     data: [65, 59, 80, 81, 56, 180, 200]
                    // },
                    // {
                    //     label: "My Third dataset",
                    //     fillColor: "#E4E3B7",
                    //     strokeColor: "#D8D7AB",
                    //     pointColor: "#B9B768",
                    //     pointStrokeColor: "#fff",
                    //     pointHighlightFill: "#fff",
                    //     pointHighlightStroke: "rgba(151,187,205,1)",
                    //     data: [28, 100, 40, 60, 50, 130, 140]
                    // }
                ]
            };
        }

        if (reviews) {        
            var data1 = {
                labels: [reviews[0][0], reviews[1][0], reviews[2][0], reviews[3][0], reviews[4][0], reviews[5][0], reviews[6][0]],
                datasets: [
                     {
                        label: "Reviews per Month",
                        fillColor: "#BBE6DC",
                        strokeColor: "#AED7CD",
                        pointColor: "#9CC5BC",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [reviews[0][1], reviews[1][1], reviews[2][1], reviews[3][1], reviews[4][1], reviews[5][1], reviews[6][1]]
                    }
                ]
            };
        }

        if (bookings) {
            var data2 = {
                labels: [bookings[0][0], bookings[1][0], bookings[2][0], bookings[3][0], bookings[4][0], bookings[5][0], bookings[6][0]],
                datasets: [
                    {
                        label: "Bookings per Month",
                        fillColor: "#E3B6EF",
                        strokeColor: "#C99DD5",
                        pointColor: "#BF99C9",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [bookings[0][1], bookings[1][1], bookings[2][1], bookings[3][1], bookings[4][1], bookings[5][1], bookings[6][1]]
                    }
                ]
            };
        }

        if (profileViews) {
            var data3 = {
                labels: [profileViews[0][0], profileViews[1][0], profileViews[2][0], profileViews[3][0], profileViews[4][0], profileViews[5][0], profileViews[6][0]],
                datasets: [
                     {
                        label: "Views of Profile",
                        fillColor: "#E4E3B7",
                        strokeColor: "#D8D7AB",
                        pointColor: "#B9B768",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [profileViews[0][1], profileViews[1][1], profileViews[2][1], profileViews[3][1], profileViews[4][1], profileViews[5][1], profileViews[6][1]]
                    }
                ]
            };
        }

        if (projectViews) {
            var data4 = {
                labels: [projectViews[0][0], projectViews[1][0], projectViews[2][0], projectViews[3][0], projectViews[4][0], projectViews[5][0], projectViews[6][0]],
                datasets: [
                     {
                        label: "Views of Projects",
                        fillColor: "#BBDBE6",
                        strokeColor: "#a8c5cf",
                        pointColor: "#95afb8",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [projectViews[0][1], projectViews[1][1], projectViews[2][1], projectViews[3][1], projectViews[4][1], projectViews[5][1], projectViews[6][1]]
                    }
                ]
            };    
        }
        

        window.onload = function(){

            var ctx     = document.getElementById("canvas_stats"),
                ctx1    = document.getElementById("canvas_reviews"),
                ctx2    = document.getElementById("canvas_bookings"),
                ctx3    = document.getElementById("canvas_profile-view"),
                ctx4    = document.getElementById("canvas_project-view");

            if (ctx) {
                window.myLine = new Chart(ctx.getContext("2d")).Line(data, {
                    responsive: true
                });
            }

            if (ctx1) {
                window.myLine1 = new Chart(ctx1.getContext("2d")).Line(data1, {
                    responsive  : true,
                    showScale   : false,
                });
            }

            if (ctx2) {
                window.myLine2 = new Chart(ctx2.getContext("2d")).Line(data2, {
                    responsive  : true,
                    showScale   : false,
                });
            }

            if (ctx3) {
                window.myLine3 = new Chart(ctx3.getContext("2d")).Line(data3, {
                    responsive  : true,
                    showScale   : false,
                });
            }

            if (ctx4) {
                window.myLine4 = new Chart(ctx4.getContext("2d")).Line(data4, {
                    responsive  : true,
                    showScale   : false,
                });
            }      
        }

    });

}


function submitForm(){

    /*
    * Global Variables
    */
    var $form       =   $(".form-card"),
        email       =   $form.find('input[type="email"]'),
        submit      =   $form.find('.btn-submit'),
        error       =   0;

    submit.click(function(){ 

        if(email.val().length != 0){ 

            $(submit).val('Submitting...').attr('disabled','disabled'); 
            
            setTimeout(
              function(){
                $form.find("input").val(''); 
                $form.find(".file-upload_result").text('No Media Chosen');
                $(submit).val('Success!').addClass('bg-success').removeAttr('disabled'); 
              }, 800);

        }else{ 

            if(error == 0){ 
                $form.find('.form-holder').addClass('shake'); 
                error = 1;
            } 

            setTimeout( 
              function(){
                $form.find('.form-holder').removeClass('shake');
               error = 0; 
              }, 3000);
        }
    });
} 