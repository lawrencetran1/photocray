// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require vendor/modernizr.min
//= require vendor/Chart.min
//= require vendor/jquery.min
//= require vendor/jquery-ui.min
//= require jquery_ujs
//= require vendor/bootstrap.min
//= require bootstrap-editable
//= require bootstrap-editable-rails
//= require data-confirm-modal
//= require dropzone
//= require filterrific/filterrific-jquery
//= require_tree .
