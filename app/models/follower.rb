class Follower < ActiveRecord::Base
  belongs_to :user

  validates :user_id, :uniqueness => {:scope => :follower_id}
  validates_numericality_of :accepted, :only_integer => true, :message => "can only be whole number."
	validates_inclusion_of :accepted, :in => 0..2, :message => "can only be between 0 and 2."
end
