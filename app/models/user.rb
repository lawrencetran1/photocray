class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :username, use: :slugged

  before_save :default_values
  before_save :to_lower
	
	mount_uploader :avatar, AvatarUploader
	mount_uploader :hero_bg, HeroBgUploader

	has_secure_password
  is_impressionable

	has_many :comments
	has_many :likes
	has_many :photos, through: :likes
	has_many :photos
	has_many :projects
	has_many :views
	has_many :followers
	has_many :follows
  has_many :reviews
  has_many :bookings
  has_many :replies
  has_many :activities

	# Validations for all users
	validates :username, presence: true, uniqueness: { case_sensitive: false }
  validates :firstname, presence: true
  validates :lastname, presence: true
	validates :email, presence: true, uniqueness: { case_sensitive: false }, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
	validates :password, presence: true, length: { in: 6..20 }, on: :create
	validates :password_confirmation, presence: true, on: :create
  validates :description, length: { maximum: 360 }
  validates_inclusion_of :country, :in => ['United States'], :message => "Sorry, only United States is supported at this time."

	def to_lower
	  self.username = self.username.downcase if self.username.present?
	  self.email = self.email.downcase if self.email.present?
    self.firstname = self.firstname.downcase if self.firstname.present?
    self.lastname = self.lastname.downcase if self.lastname.present?
    self.street = self.street.downcase if self.street.present?
    self.city = self.city.downcase if self.city.present?
    self.state = self.state.upcase if self.state.present?
	end

  def default_values
    self.rate ||= 0
  end

	self.per_page = 12

  # ----------------------------------Begin PgSearch----------------------------------------------------------

  include PgSearch
  pg_search_scope :search, 
                  :against => [:firstname, :lastname, :username],
                  :using => {
                    :tsearch => {:prefix => true}
                  }

  # ----------------------------------End PgSearch------------------------------------------------------------

  # ----------------------------------Begin Filterrific-------------------------------------------------------

  filterrific(
    available_filters: [
      :sorted_by,
      :with_category,
      :with_studio,
      :for_hire,
      :hourly_rate,
      :date_needed,
      :search_address
    ]
  )

  scope :sorted_by, lambda { |sort_option|
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^photos_count_/
      User.select("users.*, COUNT(photos.id) AS photos_count").joins(:photos).group("users.id").reorder("photos_count #{direction}")      
    when /^created_at_/
      reorder("users.created_at #{ direction }")
    when /^likes_count_/
      User.select("users.*, SUM(photos.likes_count) AS likes_count").joins(:photos).group("users.id").reorder("likes_count #{direction}")
    when /^comments_count_/
      User.select("users.*, SUM(photos.comments_count) AS comments_count").joins(:photos).group("users.id").reorder("comments_count #{direction}")
    when /^impressions_count_/
      User.select("users.*, SUM(photos.impressions_count) AS impressions_count").joins(:photos).group("users.id").reorder("impressions_count #{direction}")
		when /^followers_count_/
			User.select("users.*, SUM(followers.follower_id) AS followers_count").joins(:followers).group("users.id").reorder("followers_count #{direction}")
    when /^reviews_count_/
      User.select("users.*, SUM(reviews.reviewed_id) AS reviews_count").joins(:reviews).group("users.id").reorder("reviews_count #{direction}")
    when /^bookings_count_/
      User.select("users.*, SUM(bookings.requestor_id) AS bookings_count").joins(:bookings).group("users.id").reorder("bookings_count #{direction}")    
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }
  scope :with_category, lambda { |category|
    where('projects.category = ?', category).joins(:projects).distinct
  }
  scope :with_studio, lambda { |boolean|
    where('studio = ?', boolean)
  }
  scope :for_hire, lambda { |boolean|
    where('for_hire = ?', boolean)
  }
  scope :hourly_rate, lambda { |rate|
    where('rate <= ?', rate)
  } 
  scope :date_needed, lambda { |ref_date|
    bookings = Booking.arel_table
    users = User.arel_table
    where(
      Booking \
        .where(bookings[:user_id].eq(users[:id])) \
        .where(bookings[:date_booked].eq(ref_date)) \
        .where(bookings[:accepted_status].eq(true))
        .exists
        .not
    )
  }
  scope :search_address, lambda { |address|
    User.near(address, 50)
  }    

  def self.options_for_sorted_by
    [
      ['Member Since: Oldest to Newest', 'created_at_desc'],
      ['Member Since: Newest to Oldest', 'created_at_asc'],
      ['Total Photos: High to Low', 'photos_count_desc'],
      ['Total Photos: Low to High', 'photos_count_asc'],      
      ['Total Likes: High to Low', 'likes_count_desc'],
      ['Total Likes: Low to High', 'likes_count_asc'],
      ['Total Reviews: High to Low', 'reviews_count_desc'],
      ['Total Reviews: Low to High', 'reviews_count_asc'],      
      ['Total Bookings: High to Low', 'bookings_count_desc'],
      ['Total Bookings: Low to High', 'bookings_count_asc'],
      ['Total Comments: High to Low', 'comments_count_desc'],
      ['Total Comments: Low to High', 'comments_count_asc'],
      ['Total Views of Shots: High to Low', 'impressions_count_desc'],
      ['Total Views of Shots: Low to High', 'impressions_count_asc'],
			['Total Followers: High to Low', 'followers_count_desc'],
      ['Total Followers: Low to High', 'followers_count_asc']
    ]
  end

  def self.options_for_studio
    [
      ['Studio', true],
      ['No Studio', false]
    ]
  end

  def self.options_for_hire
    [
      ['For Hire', true],
      ['Not For Hire', false]
    ]
  end

  # ------------------------------------End Filterrific----------------------------------------------------

  # Geocoder section
  geocoded_by :full_street_address
  after_validation :geocode

  def full_street_address
    [self.street, self.city, self.state, self.country].compact.join(', ')
  end  

end
