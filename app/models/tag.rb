class Tag < ActiveRecord::Base
	has_many :tagmaps
	has_many :photos, through: :tagmaps
end
