class Project < ActiveRecord::Base
	is_impressionable
  belongs_to :user
  has_many :photos, :dependent => :destroy
  validates_inclusion_of :category, :in => %w( Weddings Birthdays Events Fashion Auto Landscape Product Portrait Food Uncategorized )
  self.per_page = 12

  # ----------------------------------Begin PgSearch----------------------------------------------------------

  include PgSearch
  pg_search_scope :search, 
                  :against => [:category],
                  :using => {
                    :tsearch => {:prefix => true}
                  }

  # ----------------------------------End PgSearch------------------------------------------------------------  

  def self.options_for_select
    [
      ['Weddings', 'Weddings'],
      ['Birthdays', 'Birthdays'],
      ['Events', 'Events'],
      ['Fashion', 'Fashion'],
      ['Auto', 'Auto'],
      ['Landscape', 'Landscape'],
      ['Product', 'Product'],
      ['Portrait', 'Portrait'],
      ['Food', 'Food'],
      ['Uncategorized', 'Uncategorized']
    ]
  end
end
