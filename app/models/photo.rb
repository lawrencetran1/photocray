class Photo < ActiveRecord::Base
  attr_accessor :original_width, :original_height

  belongs_to :user
  belongs_to :project

  has_many :likes, :dependent => :destroy
  has_many :users, through: :likes
  has_many :comments, :dependent => :destroy
  has_many :views
  has_many :tagmaps
  has_many :tags, through: :tagmaps
  has_many :bookings, through: :user

  # Creates a impressions_count column in the photos table
  # :request_increments by 1 each time the photo show page is loaded
  is_impressionable :counter_cache => true, :column_name => :impressions_count, :unique => :request_hash

  # Carrierwave gem, filename is saved under the original column of the photos table
	mount_uploader :original, PhotoUploader

  self.per_page = 12

  # ----------------------------------Begin Filterrific-----------------------------------------------

  filterrific(
    available_filters: [
      :sorted_by,
      :with_category,
      :with_studio,
      :for_hire,
      :hourly_rate,
      :date_needed,
      :search_address
    ]
  )

  scope :sorted_by, lambda { |sort_option|
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at_/
      reorder("photos.created_at #{ direction }")
    when /^likes_count_/
      reorder("photos.likes_count #{ direction }")
    when /^comments_count_/
      reorder("photos.comments_count #{ direction }")
    when /^impressions_count_/
      reorder("photos.impressions_count #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }
  scope :with_category, lambda { |category|
    where('projects.category = ?', category).joins(:project)
  }
  scope :with_studio, lambda { |boolean|
    where('users.studio = ?', boolean).joins(:user)
  }
  scope :for_hire, lambda { |boolean|
    where('users.for_hire = ?', boolean).joins(:user)
  }
  scope :hourly_rate, lambda { |rate|
    where('users.rate <= ?', rate).joins(:user)
  }
  scope :date_needed, lambda { |ref_date|
    bookings = Booking.arel_table
    users = User.arel_table
    where(
      Booking \
        .where(bookings[:user_id].eq(users[:id])) \
        .where(bookings[:date_booked].eq(ref_date)) \
        .where(bookings[:accepted_status].eq(true))
        .exists
        .not
    )  
  }
  scope :search_address, lambda { |address|
    near = User.near(address, 50)
    Photo.includes(:user).references(:user).merge(near)
  }   

  # ----------------------------------End Filterrific-----------------------------------------------

  # Call check_dimensions when a photo is uploaded, minimum resolution is 1600 by 1200
  validate :check_dimensions, :on => :create

  def check_dimensions
    if !original_cache.nil? && (original.width < 1600 || original.height < 1200)
      errors.add :original, "Dimension too small."
    end
  end

  def self.options_for_sorted_by
    [
      ['Recent', 'created_at_desc'],
      ['Oldest', 'created_at_asc'],
      ['Most Likes', 'likes_count_desc'],
      ['Most Comments', 'comments_count_desc'],
      ['Most Views', 'impressions_count_desc']
    ]
  end

end
