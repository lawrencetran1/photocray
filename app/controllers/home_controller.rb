class HomeController < ApplicationController
  def meetups
  end

  def jobs
  end

  def about
  end

  def contests
  end

  def blog
  end
end
