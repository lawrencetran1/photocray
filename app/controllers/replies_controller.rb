class RepliesController < ApplicationController
	before_action :require_user, only: :create

  def create
    @user = User.friendly.find(params[:user_id])
    if @user.id == current_user.id
	    Reply.create(:user_id => @user.id, :reviewer_id => params[:reviewer_id], :body => params[:body])
	    redirect_to :back
    end
  end

  def update
  	reply = Reply.find(params[:id])
  	reply.body = params[:body]
  	if reply.save
  		redirect_to :back
  	end
  end
end
