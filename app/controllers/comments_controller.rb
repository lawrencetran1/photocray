class CommentsController < ApplicationController
	before_action :require_user, only: :create
	
  def create
    @comment = Comment.new(params.require(:comment).permit(:comment, :photo_id, :user_id))
    @comment.photo_id = params[:photo_id]
    @comment.user_id = current_user.id
    @comment.save
    Activity.create(:user_id => User.find(Photo.find(params[:photo_id]).user_id).id, :activity_type => 'comment', :from => current_user.id, :activity_id => params[:photo_id])
    redirect_to photo_path(params[:photo_id])
  end
end
