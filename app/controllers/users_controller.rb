class UsersController < ApplicationController
  require 'will_paginate'
  before_action :require_user, only: [:update, :add_follow, :unfollow, :upload_avatar, :upload_hero_bg, :analytics, :activity]
  impressionist actions: [:show]

  def index
    @filterrific = initialize_filterrific(
      User,
      params[:filterrific],
      select_options: {
        sorted_by: User.options_for_sorted_by,
        with_category: Project.options_for_select,
        with_studio: User.options_for_studio,
        for_hire: User.options_for_hire
      }
    ) or return

    user_ids = []
    users = User.all
    users.each do |user|
      # exclude users that have no photos
      if user.photos.count > 0
        user_ids << user.id
      end
    end

    @users = @filterrific.find.page(params[:page]).where(:id => user_ids)

    if params[:filterrific]
      @location = Geocoder.coordinates(params[:filterrific][:search_address])
    end

    respond_to do |format|
      format.html
      format.js
    end

  end

  def new
    @user = User.new
  end

  def edit
    @user = User.friendly.find(params[:id])
  end

  def update
    @user = User.friendly.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to user_reviews_path(current_user) if !current_user.is_photographer
      redirect_to user_path(current_user) if current_user.is_photographer
    else
      flash[:alert] = @user.errors.full_messages
      render 'edit'
    end
  end

  def show
    @user = User.friendly.find(params[:id])
    @photos = @user.photos.page(params[:page])
    @title = @user.is_photographer? ? "#{@user.firstname.titleize} #{@user.lastname.titleize} - Photographer" : "#{@user.firstname.titleize} #{@user.lastname.titleize} - Client"
    @description = @user.description.truncate(150) if @user.description.present?
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
    impressionist(@user)
    redirect_to user_reviews_path(@user) if !@user.is_photographer
  end

  def following
    @user = User.friendly.find(params[:user_id])
    @ids = []
    @user.follows.each do |x|
      @ids << x.followed_id
    end
    @follows = User.where(:id => @ids).page(params[:page])
    @title = @user.is_photographer? ? "#{@user.firstname.titleize} #{@user.lastname.titleize} - Photographer Friends" : "#{@user.firstname.titleize} #{@user.lastname.titleize} - Client Friends"
    @description = @user.description.truncate(150) if @user.description.present?
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
  end

  def followers
    @user = User.friendly.find(params[:user_id])
    @ids = []
    @user.followers.each do |x|
      @ids << x.follower_id
    end
    @follows = User.where(:id => @ids).page(params[:page])
    @title = @user.is_photographer? ? "#{@user.firstname.titleize} #{@user.lastname.titleize} - Photographer Followers" : "#{@user.firstname.titleize} #{@user.lastname.titleize} - Client Followers"
    @description = @user.description.truncate(150) if @user.description.present?
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
  end  

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id.to_s
      redirect_to edit_user_path(current_user)
    else
      flash[:alert] = @user.errors.full_messages
      render 'new'
    end
  end

  def add_follow
    @user = User.find(current_user.id)
    @followed = User.find(params[:followed_id])
    @user.follows.create(:user_id => current_user.id, :followed_id => params[:followed_id])
    @followed.followers.create(:user_id => @followed.id, :follower_id => current_user.id, :accepted => '1')
    Activity.create(:user_id => @followed.id, :activity_type => 'follower', :from => current_user.id)
    redirect_to :back
  end

  def unfollow
    @user1 = User.find(current_user.id)
    @follow = @user1.follows.find_by(:followed_id => params[:followed_id])
    @follow.destroy

    @user2 = User.find(params[:followed_id])
    @follower = @user2.followers.find_by(:follower_id => params[:follower_id])
    @follower.destroy

    redirect_to :back
  end

  def upload_avatar
    @user = User.find(current_user.id)
    @user.avatar = params[:avatar]
    if @user.save
      redirect_to :back
    else
      flash[:alert] = 'Only .jpg and .jpeg allowed'
      redirect_to :back
    end
  end

  def upload_hero_bg
    @user = User.find(current_user.id)
    @user.hero_bg = params[:hero_bg]
    if @user.save
      redirect_to :back
    else
      flash[:alert] = 'Only .jpg and .jpeg allowed'
      redirect_to :back
    end
  end

  def analytics
    @user = User.find(current_user.id)
    @title = @user.is_photographer? ? "#{@user.firstname.titleize} #{@user.lastname.titleize} - Photographer Analytics" : "#{@user.firstname.titleize} #{@user.lastname.titleize} - Client Analytics"
    @description = @user.description.truncate(150) if @user.description.present?
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
  end

  def activity
    @user = User.find(current_user.id)
    @title = @user.is_photographer? ? "#{@user.firstname.titleize} #{@user.lastname.titleize} - Photographer Activity" : "#{@user.firstname.titleize} #{@user.lastname.titleize} - Client Activity"
    @description = @user.description.truncate(150) if @user.description.present?
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
  end  

  private

  def user_params
    params.require(:user).permit(:firstname, :lastname, :street, :city, :state, :country, :rate, :is_photographer, :for_hire, :studio, :description, :username, :email, :password, :password_confirmation)
  end
end
