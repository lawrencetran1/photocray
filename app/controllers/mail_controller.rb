class MailController < ApplicationController
	def get_quote
			email_to = User.friendly.find(params[:user_id]).email
	    email_from = params[:email_from]
	    subject = params[:subject]
	    message = params[:message]
	    UserMailer.get_quote(email_to, email_from, subject, message).deliver
	    redirect_to :back, notice: 'Message sent'
	end	
end
