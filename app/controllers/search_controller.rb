class SearchController < ApplicationController

	def index
	  if params[:query]
	  	project_ids = []
	  	user_ids = []
	    users = User.search(params[:query])
	    users.each do |user|
	    	# exclude users that have no photos
	    	if user.photos.count > 0
	    		user_ids << user.id
    		end
    	end
    	# search all projects where params[:query] matches project category and display all corresponding photos
	    projects = Project.search(params[:query])
	    projects.each do |project|
	    	project_ids << project.id
    	end
    	@users = User.where(:id => user_ids).order(:created_at => :desc).page(params[:page])
    	@photos = Photo.where(:project_id => project_ids).order("likes_count DESC").page(params[:page])
	  else
	    @users = nil
	    @photos = nil
	  end		
	end

	def search_post
	  redirect_to search_path(params[:query])
	end	

end
