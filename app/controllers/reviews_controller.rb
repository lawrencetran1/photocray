class ReviewsController < ApplicationController
	require 'will_paginate'
  before_action :require_user, only: :create

  def index
  	@user = User.friendly.find(params[:user_id])
  	@reviews = @user.reviews.count > 0 ? @user.reviews.order(:created_at => :desc).group_by { |review| review.reviewed_id } : []
    @title = "#{@user.firstname.titleize} #{@user.lastname.titleize} - Photographer Reviews"
    @description = @user.description.truncate(150)
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
    @replies = @user.replies
  end

  def create
    @user = User.friendly.find(params[:user_id])
    if @user.id != current_user.id
	    Review.create(:user_id => @user.id, :reviewed_id => current_user.id, :body => params[:body])
      Activity.create(:user_id => @user.id, :activity_type => 'review', :from => current_user.id)
	    redirect_to user_reviews_path(@user)
    end
  end

end
