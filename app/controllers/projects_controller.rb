class ProjectsController < ApplicationController
  require 'will_paginate'
  before_action :require_user, only: [:add_project, :create, :destroy, :update]
  impressionist :actions=>[:show]
  
  def index
  	@user = User.friendly.find(params[:user_id])
    @projects = @user.projects.page(params[:page])
    @title = "#{@user.firstname.titleize} #{@user.lastname.titleize} - Professional Albums"
    @description = @user.description.truncate(150) if @user.description.present?
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
    redirect_to user_reviews_path(@user) if !@user.is_photographer
  end

  def show
  	@user = User.friendly.find(params[:user_id])
    @project = Project.find(params[:id])
    @title = @project.title.present? ? "#{@user.firstname.titleize} #{@user.lastname.titleize} - #{@project.title} Album" :  "#{@user.firstname.titleize} #{@user.lastname.titleize} - Untitled Album"
    @description = @user.description.truncate(150) if @user.description.present?
    @og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
    impressionist(@project)
    @photos = @user.projects.find_by_id(params[:id]).photos.order(:created_at => :desc).page(params[:page])
  end

  def add_project
    @project = Project.new
    @project.user_id = current_user.id
    @project.title = "Untitled"
    @project.save
    redirect_to user_projects_path(current_user)
  end

  def create
    @project = Project.new
    @project.user_id = current_user.id
    @project.title = "Untitled"
    @project.save
    redirect_to user_projects_path(current_user)
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    redirect_to :back      
  end

  def update
    @project = Project.find(params[:id])

    respond_to do |format|
      if @project.update_attributes(project_params)
        format.html { redirect_to :back, notice: 'Project was successfully updated.' }
        format.json { head :no_content } # 204 No Content
      else
        format.html { render action: "index" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    def project_params
      params.require(:project).permit(:title, :category)
    end   

end
