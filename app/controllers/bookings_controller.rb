class BookingsController < ApplicationController
	require 'will_paginate'
	before_action :require_user

	def index
		@user = User.friendly.find(params[:user_id])
		@bookings = Booking.where("user_id = ? OR requestor_id = ?", @user.id, @user.id).order(:created_at => :desc)
		@description = @user.description.truncate(150)
		@title = "#{@user.firstname.titleize} #{@user.lastname.titleize} - Photographer Reviews"
		@og_image = @user.avatar.present? ? request.base_url + @user.avatar_url : request.base_url + '/assets/user_profiles/default.png'
		redirect_to root_path if @user != current_user
	end

	def create
    @user = User.friendly.find(params[:user_id])
    date = Date.strptime(params[:date], '%m/%d/%Y')
    if @user.id != current_user.id
	    Booking.create(:user_id => @user.id, :requestor_id => current_user.id, :date_booked => date, :accepted_status => nil)
	    Activity.create(:user_id => @user.id, :activity_type => 'booking', :from => current_user.id)
	    redirect_to user_bookings_path(current_user)
    end		
	end

	def update
		booking = Booking.find(params[:booking_id])
		booking.accepted_status = params[:accepted_status]
		if booking.save
			redirect_to :back
		end
	end

end
