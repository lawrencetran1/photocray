class PhotosController < ApplicationController
	require 'will_paginate'
	before_action :require_user, only: [:like, :unlike, :destroy]
	impressionist :actions=>[:show]

	def index
    @filterrific = initialize_filterrific(
      Photo,
      params[:filterrific],
      :select_options => {
        sorted_by: Photo.options_for_sorted_by,
        with_category: Project.options_for_select,
        with_studio: User.options_for_studio,
        for_hire: User.options_for_hire
      }
    ) or return
    @photos = @filterrific.find.page(params[:page])

    if params[:filterrific]
      @location = Geocoder.coordinates(params[:filterrific][:search_address])
    end    

    respond_to do |format|
      format.html
      format.js
    end
	end

	def new
		@photo = Photo.new
	end

  def edit
    @photo = Photo.find(params[:id])
  end	

	def show
		@photo = Photo.find(params[:id])
		impressionist(@photo)

		@title =  @photo.title.present? ? "#{@photo.user.firstname.titleize} #{@photo.user.lastname.titleize} - #{@photo.title}" : "#{@photo.user.firstname.titleize} #{@photo.user.lastname.titleize} - Untitled"

		@description = @photo.description.truncate(150) if @photo.description.present?

		@og_image = request.base_url + @photo.original_url

		# related photos (same category)
		@photos = Photo.joins(:project).where("projects.category = ?", Project.find(@photo.project_id).category).where.not(:id => @photo.id)

		# get 2 random photos from related photos
		if @photos.count > 1
			rand_nums = (0..@photos.length - 1).to_a.shuffle
			@first = @photos[rand_nums[0]]
			@second = @photos[rand_nums[1]]
		elsif @photos.count == 1
			@first = @photos[0]
		end
		@fullname = "#{@photo.user.firstname.capitalize} #{@photo.user.lastname.capitalize}"
	end

	def fullsize
		@fullsize = Photo.find(params[:photo_id])
		@fullname = "#{@fullsize.user.firstname.capitalize} #{@fullsize.user.lastname.capitalize}"
		@title =  @fullsize.title.present? ? "#{@fullsize.user.firstname.titleize} #{@fullsize.user.lastname.titleize} - #{@fullsize.title}" : "#{@fullsize.user.firstname.titleize} #{@fullsize.user.lastname.titleize} - Untitled"

		@description = @fullsize.description.truncate(150) if @fullsize.description.present?
		@og_image = request.base_url + @fullsize.original_url
	end

	def like
		Like.create(:user_id => current_user.id, :photo_id => params[:photo])
		Activity.create(:user_id => User.find(Photo.find(params[:photo]).user_id).id, :activity_type => 'like', :from => current_user.id, :activity_id => params[:photo])
		redirect_to :back
	end

	def unlike
		@photo = Photo.find(params[:photo])
		@like = @photo.likes.find_by(user_id: current_user.id)
		@like.destroy
		redirect_to :back
	end	

	def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    redirect_to :back
	end

  def update
    @photo = Photo.find(params[:id])

    respond_to do |format|
      if @photo.update_attributes(photo_params)
        format.html { redirect_to :back, notice: 'Photo was successfully updated.' }
        format.json { head :no_content } # 204 No Content
      else
        format.html { render action: "show" }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end	

  private
    def photo_params
      params.require(:photo).permit(:title, :description)
    end 

end
