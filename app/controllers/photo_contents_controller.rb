class PhotoContentsController < ApplicationController
	before_action :require_user, only: :create
	
  def create
    @photo = Photo.new(original: params[:file], project_id: params[:project_id])
    @photo.user_id = current_user.id
    if @photo.save!
      respond_to do |format|
        format.json{ render :json => @photo }
      end
    end
  end
end
