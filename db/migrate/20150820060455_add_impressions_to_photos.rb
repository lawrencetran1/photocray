class AddImpressionsToPhotos < ActiveRecord::Migration
  def change
  	add_column :photos, :impressions_count, :integer, :default => 0
  end
end
