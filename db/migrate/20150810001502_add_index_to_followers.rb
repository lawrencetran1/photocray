class AddIndexToFollowers < ActiveRecord::Migration
  def change
  	add_index :followers, [:user_id, :follower_id], :unique => true
  end
end
