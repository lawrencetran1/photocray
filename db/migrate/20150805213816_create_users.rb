class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :email
      t.string :firstname
      t.string :lastname
      t.string :password_digest
      t.string :hero_bg
      t.string :street
      t.string :city
      t.string :state
      t.string :country, :default => 'United States'
      t.boolean :is_photographer
      t.boolean :for_hire
      t.boolean :studio
      t.string :description
      t.integer :rate

      t.timestamps null: false
    end
  end
end
