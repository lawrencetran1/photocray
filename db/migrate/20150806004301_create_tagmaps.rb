class CreateTagmaps < ActiveRecord::Migration
  def change
    create_table :tagmaps do |t|
      t.references :photo, index: true, foreign_key: true
      t.references :tag, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
