class AddIndexToReplies < ActiveRecord::Migration
  def change
  	add_index :replies, [:user_id, :reviewer_id], :unique => true
  end
end
