class AddIndexToFollows < ActiveRecord::Migration
  def change
  	add_index :follows, [:user_id, :followed_id], :unique => true
  end
end
