class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :requestor_id
      t.date :date_booked
      t.boolean :accepted_status, :default => nil, :null => true

      t.timestamps null: false
    end
  end
end
