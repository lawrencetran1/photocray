class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :reviewed_id
      t.string :body

      t.timestamps null: false
    end
  end
end
