User.create(username:"larry", email:"larry@larry.com", firstname: "larry", lastname: "tran", password: "password", password_confirmation: "password", street: "15951 Ward Street", city: "Westminster", state: "CA", country: "United States", is_photographer: true, for_hire: true, studio: false, rate: 200, description: "I'm a photographer with 15 years of experience. I specialize in architecture, travel, product and environmental portraiture. Making beautiful images is my passion. I enjoy the process of working with clients to understand their goals. Small or large, each job is equal to me, and I approach each one with my enthusiasm, passion and experience for photography.")
User.create(username:"thien", email:"thien@thien.com", firstname: "thien", lastname: "pham", password: "password", password_confirmation: "password", street: "1918 S Harvard Blvd", city: "Los Angeles", state: "CA", country: "United States", is_photographer: true, for_hire: true, studio: true, rate: 190, description: "I'm a photographer with 15 years of experience. I specialize in architecture, travel, product and environmental portraiture. Making beautiful images is my passion. I enjoy the process of working with clients to understand their goals. Small or large, each job is equal to me, and I approach each one with my enthusiasm, passion and experience for photography.")
User.create(username:"anthony", email:"anthony@anthony.com", firstname: "anthony", lastname: "ha", password: "password", password_confirmation: "password", street: "1800 Cooper St", city: "Santa Ana", state: "CA", country: "United States", is_photographer: true, for_hire: true, studio: true, rate: 180, description: "I'm a photographer with 15 years of experience. I specialize in architecture, travel, product and environmental portraiture. Making beautiful images is my passion. I enjoy the process of working with clients to understand their goals. Small or large, each job is equal to me, and I approach each one with my enthusiasm, passion and experience for photography.")
User.create(username:"phong", email:"phong@phong.com", firstname: "phong", lastname: "quan", password: "password", password_confirmation: "password", street: "1600 Tampion Ave", city: "Garden Grove", state: "CA", country: "United States", is_photographer: true, for_hire: true, studio: true, rate: 200, description: "I'm a photographer with 15 years of experience. I specialize in architecture, travel, product and environmental portraiture. Making beautiful images is my passion. I enjoy the process of working with clients to understand their goals. Small or large, each job is equal to me, and I approach each one with my enthusiasm, passion and experience for photography.")

# Create 46 random users
46.times do |i|
	User.create(username:"user#{i}", email:"user#{i}@user#{i}.com", firstname: "user#{i}", lastname: "user#{i}", password: "password", password_confirmation: "password", street: "12#{i} Fake Street", city: "Fake City", state: "CA", country: "United States", is_photographer: true, for_hire: true, studio: false, rate: 200, description: "I'm a photographer with 15 years of experience. I specialize in architecture, travel, product and environmental portraiture. Making beautiful images is my passion. I enjoy the process of working with clients to understand their goals. Small or large, each job is equal to me, and I approach each one with my enthusiasm, passion and experience for photography.")
end

Follow.create(user_id: 1, followed_id: 2)
Follow.create(user_id: 1, followed_id: 3)
Follow.create(user_id: 1, followed_id: 4)
Follower.create(user_id: 1, follower_id: 4, accepted: 1)

Follow.create(user_id: 2, followed_id: 1)
Follow.create(user_id: 2, followed_id: 3)
Follower.create(user_id: 2, follower_id: 1, accepted: 1)
Follower.create(user_id: 2, follower_id: 4, accepted: 1)

Follow.create(user_id: 3, followed_id: 4)
Follower.create(user_id: 3, follower_id: 2, accepted: 1)
Follower.create(user_id: 3, follower_id: 1, accepted: 1)
Follower.create(user_id: 3, follower_id: 4, accepted: 1)

Follow.create(user_id: 4, followed_id: 2)
Follow.create(user_id: 4, followed_id: 3)
Follow.create(user_id: 4, followed_id: 1)
Follower.create(user_id: 4, follower_id: 2, accepted: 1)
Follower.create(user_id: 4, follower_id: 3, accepted: 1)
Follower.create(user_id: 4, follower_id: 1, accepted: 1)

Project.create(title:"Beautiful Wedding", category:"Weddings", user_id: 1)
Project.create(title:"Awesome Birthday", category:"Birthdays", user_id: 2)
Project.create(title:"Good Times", category:"Events", user_id: 3)
Project.create(title:"Still Life", category:"Food", user_id: 4)

wedding1 = Photo.create!(user_id: 1, project_id: 1)
wedding1.original = Rails.root.join("app/assets/images/photos/wedding1.jpg").open
wedding1.save!

wedding2 = Photo.create!(user_id: 1, project_id: 1)
wedding2.original = Rails.root.join("app/assets/images/photos/wedding2.jpg").open
wedding2.save!

wedding3 = Photo.create!(user_id: 1, project_id: 1)
wedding3.original = Rails.root.join("app/assets/images/photos/wedding3.jpg").open
wedding3.save!

wedding4 = Photo.create!(user_id: 1, project_id: 1)
wedding4.original = Rails.root.join("app/assets/images/photos/wedding4.jpg").open
wedding4.save!

wedding5 = Photo.create!(user_id: 1, project_id: 1)
wedding5.original = Rails.root.join("app/assets/images/photos/wedding5.jpg").open
wedding5.save!

wedding6 = Photo.create!(user_id: 1, project_id: 1)
wedding6.original = Rails.root.join("app/assets/images/photos/wedding6.jpg").open
wedding6.save!

wedding7 = Photo.create!(user_id: 1, project_id: 1)
wedding7.original = Rails.root.join("app/assets/images/photos/wedding7.jpg").open
wedding7.save!

birthday1 = Photo.create!(user_id: 2, project_id: 2)
birthday1.original = Rails.root.join("app/assets/images/photos/birthday1.jpg").open
birthday1.save!

birthday2 = Photo.create!(user_id: 2, project_id: 2)
birthday2.original = Rails.root.join("app/assets/images/photos/birthday2.jpg").open
birthday2.save!

event1 = Photo.create!(user_id: 3, project_id: 3)
event1.original = Rails.root.join("app/assets/images/photos/event1.jpg").open
event1.save!

event2 = Photo.create!(user_id: 3, project_id: 3)
event2.original = Rails.root.join("app/assets/images/photos/event2.jpg").open
event2.save!

event3 = Photo.create!(user_id: 3, project_id: 3)
event3.original = Rails.root.join("app/assets/images/photos/event3.jpg").open
event3.save!

event4 = Photo.create!(user_id: 3, project_id: 3)
event4.original = Rails.root.join("app/assets/images/photos/event4.jpg").open
event4.save!

event5 = Photo.create!(user_id: 3, project_id: 3)
event5.original = Rails.root.join("app/assets/images/photos/event5.jpg").open
event5.save!

event6 = Photo.create!(user_id: 3, project_id: 3)
event6.original = Rails.root.join("app/assets/images/photos/event6.jpg").open
event6.save!

food1 = Photo.create!(user_id: 4, project_id: 4)
food1.original = Rails.root.join("app/assets/images/photos/food1.jpg").open
food1.save!

food2 = Photo.create!(user_id: 4, project_id: 4)
food2.original = Rails.root.join("app/assets/images/photos/food2.jpg").open
food2.save!

food3 = Photo.create!(user_id: 4, project_id: 4)
food3.original = Rails.root.join("app/assets/images/photos/food3.jpg").open
food3.save!

food4 = Photo.create!(user_id: 4, project_id: 4)
food4.original = Rails.root.join("app/assets/images/photos/food4.jpg").open
food4.save!

(1..19).each do |i|
	Like.create(user_id: 1, photo_id: i)
	Comment.create(user_id: 2, photo_id: i, comment: 'nice photo!')
	Comment.create(user_id: 3, photo_id: i, comment: 'sweet image!')
	Comment.create(user_id: 4, photo_id: i, comment: 'dope pic man')
end

(3..15).each do |i|
	Like.create(user_id: 2, photo_id: i)
	Comment.create(user_id: 1, photo_id: i, comment: 'i like this photo')
	Comment.create(user_id: 3, photo_id: i, comment: 'me too!, this photo is too good!')
	Comment.create(user_id: 4, photo_id: i, comment: 'its pretty good...what kind of camera did you use?')
end

(5..13).each do |i|
	Like.create(user_id: 3, photo_id: i)
	Comment.create(user_id: 3, photo_id: i, comment: 'hi i am a comment')
end

(7..11).each do |i|
	Like.create(user_id: 4, photo_id: i)
	Comment.create(user_id: 4, photo_id: i, comment: 'hi i am a comment')
end

(5..50).each do |i|
	(1..19).each do |j|
		Like.create(user_id: i, photo_id: j, created_at: Time.now - Random.rand(210).days)
	end
end

50.times do |i|
	random1 = Time.now - Random.rand(210).days
	random2 = Time.now - Random.rand(210).days
	random3 = Time.now - Random.rand(210).days
	random4 = Time.now - Random.rand(210).days

	Booking.create(user_id: 1, requestor_id: ([*1..50] - [1]).sample, date_booked: random1, created_at: random1, accepted_status: true)
	Booking.create(user_id: 2, requestor_id: ([*1..50] - [2]).sample, date_booked: random2, created_at: random2, accepted_status: true)
	Booking.create(user_id: 3, requestor_id: ([*1..50] - [3]).sample, date_booked: random3, created_at: random3, accepted_status: true)
	Booking.create(user_id: 4, requestor_id: ([*1..50] - [4]).sample, date_booked: random4, created_at: random4, accepted_status: true)

	Review.create(user_id: 1, reviewed_id: ([*1..50] - [1]).sample, created_at: Time.now - Random.rand(210).days, body: 'hi i am a review')
	Review.create(user_id: 2, reviewed_id: ([*1..50] - [2]).sample, created_at: Time.now - Random.rand(210).days, body: 'hi i am a review')
	Review.create(user_id: 3, reviewed_id: ([*1..50] - [3]).sample, created_at: Time.now - Random.rand(210).days, body: 'hi i am a review')
	Review.create(user_id: 4, reviewed_id: ([*1..50] - [4]).sample, created_at: Time.now - Random.rand(210).days, body: 'hi i am a review')

	# Create profile view impressions
	Impression.create(impressionable_type: 'User', impressionable_id: 1, user_id: Random.rand(2..4), controller_name: 'users', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'User', impressionable_id: 2, user_id: ([*1..4] - [2]).sample, controller_name: 'users', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'User', impressionable_id: 3, user_id: ([*1..4] - [3]).sample, controller_name: 'users', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'User', impressionable_id: 4, user_id: Random.rand(1..4), controller_name: 'users', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users", created_at: Time.now - Random.rand(210).days)

	# Create project view impressions
	Impression.create(impressionable_type: 'Project', impressionable_id: 1, user_id: Random.rand(2..4), controller_name: 'projects', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users/1/projects", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Project', impressionable_id: 2, user_id: ([*1..4] - [2]).sample, controller_name: 'projects', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users/2/projects", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Project', impressionable_id: 3, user_id: ([*1..4] - [3]).sample, controller_name: 'projects', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users/3/projects", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Project', impressionable_id: 4, user_id: Random.rand(1..4), controller_name: 'projects', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/users/4/projects", created_at: Time.now - Random.rand(210).days)

	# Create photo view impressions
	Impression.create(impressionable_type: 'Photo', impressionable_id: 1, user_id: 1, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 2, user_id: 1, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 3, user_id: 1, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 4, user_id: 1, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 5, user_id: 1, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 6, user_id: 1, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 7, user_id: 1, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 8, user_id: 2, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 9, user_id: 2, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 10, user_id: 3, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 11, user_id: 3, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 12, user_id: 3, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 13, user_id: 3, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 14, user_id: 3, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 15, user_id: 3, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 16, user_id: 4, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 17, user_id: 4, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 18, user_id: 4, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
	Impression.create(impressionable_type: 'Photo', impressionable_id: 19, user_id: 4, controller_name: 'photos', action_name: 'show', request_hash: SecureRandom.hex + SecureRandom.hex, ip_address: "::1", session_hash: SecureRandom.hex, referrer: "http://localhost:3000/", created_at: Time.now - Random.rand(210).days)
end
