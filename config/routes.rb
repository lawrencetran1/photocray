Rails.application.routes.draw do

  get 'home/meetups'

  get 'home/jobs'

  get 'home/about'

  get 'home/contests'

  get 'home/blog'

  resources :users do
    get 'following' => 'users#following'
    get 'followers' => 'users#followers'
    get 'analytics' => 'users#analytics'
    get 'activity' => 'users#activity'
    post 'get_quote' => 'mail#get_quote'
    resources :projects, :except => [:new]
    resources :reviews, :only => [:index, :create]
    resources :replies, :only => [:create, :update]
    resources :bookings, :only => [:index, :create, :update]
  end

  resources :photos do
    get 'fullsize' => 'photos#fullsize'
    resources :comments, :only => [:create]
  end

  resources :photo_contents, :only => [:create]
  resources :search, :only => [:index]

  put '/users/:user_id/replies/:id', :to => 'replies#update', :as => 'update_reply'
  put '/users/:id/bookings', :to => 'bookings#update', :as => 'update_booking'
  post '/upload_avatar' => 'users#upload_avatar'
  post '/upload_hero_bg' => 'users#upload_hero_bg'
  post '/add_project' => 'projects#add_project'
  post '/add_follow' => 'users#add_follow'
  delete '/unfollow' => 'users#unfollow'
  post '/like' => 'photos#like'
  delete '/unlike' => 'photos#unlike'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'
  
  root :to =>'photos#index'

end
