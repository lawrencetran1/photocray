# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.photocray.com"
SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new
SitemapGenerator::Sitemap.sitemaps_host = ENV['SITEMAP_HOST']

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
  add photos_path
  add users_path
  add home_meetups_path
  add home_jobs_path
  add home_about_path
  add home_contests_path
  add home_blog_path

  Photo.find_each do |photo|
    add photo_path(photo)
    add photo_fullsize_path(photo)
  end

  User.find_each do |user|
    add user_path(user)
    add user_following_path(user)
    add user_followers_path(user)
    add user_projects_path(user)
    add user_reviews_path(user)

    user.projects.find_each do |project|
      add user_project_path(user, project)
    end
  end

end

SitemapGenerator::Sitemap.ping_search_engines